window.cookieManager = window.cookieManager | {};

function isUserAcceptingCookies() {
    return Math.random() <= 0.5;;
}

function getCookie(name) {
    return 'Cookie: ' + name;
}

function setCookie(name, value, expiryDate, isEssentialCookie) {
    if (isEssentialCookie) {
        return 'Essential cookie name: ' + name + ', value: ' + value + ', expiryDate: ' + expiryDate
    } else if (isUserAcceptingCookies()) {
        return 'Cookie name: ' + name + ', value: ' + value + ', expiryDate: ' + expiryDate
    }
}

function deleteCookie(name) {
    return 'Cookie ' + name + ' was deleted'
}

window.cookieManager.getCookie = getCookie;
window.cookieManager.setCookie = setCookie;
window.cookieManager.deleteCookie = deleteCookie;